#!/bin/sh

SCRIPT_DIR=$(dirname $0;)
iconv -f=CP1251 -t=UTF-8 "$1" | awk -f "$SCRIPT_DIR/VTB_convert.awk"