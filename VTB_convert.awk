BEGIN {FS=";"; OFS=","; print "Date,Payee,Memo,Amount"}
NF==9 {if ($1 !~ /^Номер/) 
{
    sum = gensub(/ /, "", "g", gensub(/,/, ".", "g", $6))
    payee = gensub(/,/, "_", "g", $8)
    print gensub(/(\S+) (\S+)/, "\\1", "g", $2), payee, "", sum 
}}